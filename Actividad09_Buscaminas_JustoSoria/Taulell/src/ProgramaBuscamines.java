import java.util.Scanner;

public class ProgramaBuscamines {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Taulell t = new Taulell();
        Finestra f = new Finestra(t);
        
        Scanner sc = new Scanner(System.in);
        
        sc.nextLine();
        
        //cas 2: Buscaminas
        
        System.out.println("cas 2: Text - Buscamines");
        
        t.setActcolors(false);
        t.setFons(0xb1adad);
        t.setActimatges(false);
        t.setActlletres(true);
        String[] lletres = {"","1","2","3","4","5","6","7","8","*"};  //qu� s'ha d'escriure en cada casella en base al nombre
        t.setLletres(lletres);
        int[] colorlletres = {0x0000FF,0x00FF00,0xFFFF00,0xFF0000,0xFF00FF,0x00FFFF,0x521b98,0xFFFFFF,0xFF8000,0x7F00FF};
        t.setColorlletres(colorlletres);
        String[] etiquetes2={"Mines: 10", "Temps: 600"};
        f.setEtiquetes(etiquetes2);
        f.setActetiquetes(true);
        f.setTitle("Cercamines");
        
        int[][] matriu2 =
        	{
        		{9,9,9,9,9,9,9,9,9,9},
        		{9,9,1,1,2,9,9,5,9,9},
        		{9,9,9,9,3,9,9,9,9,9},
        		{9,9,5,9,6,9,9,9,9,9},
        		{9,9,9,9,6,9,5,9,9,9},
        		{9,9,8,9,6,9,9,9,6,9},
        		{9,9,5,9,6,9,9,9,9,9},
        		{1,1,1,9,9,9,9,9,9,9},
        		{0,0,1,2,9,9,9,9,9,9},
        		{0,0,0,1,9,4,1,7,8,9},
        	};
        		
        t.dibuixa(matriu2);
        sc.nextLine();
        
        System.out.println("cas 6: Comprovacio mouse i teclat");
        
        System.out.println("prem en una casella amb el ratol� i apreta una tecla del teclat. Despr�s prem intro");
        
        sc.nextLine();
        System.out.println("l'ultima casella clickada es:  fila "+t.getMousefil()+"   columna: "+t.getMousecol());
        System.out.println("l'ultima tecla premuda es:   "+f.getActualChar());
        System.out.println("ara hauria d'estar en zero. Aixo reflecteix les tecles"
        		+ "que vols apretar i un cop actuades no vols que quedin en memoria:  "+f.getActualChar());
        System.out.println("i aquesta es l'�ltima tecla premuda sense quedarse en blanc: Aix� �s per quan"
        		+ "vols un sistema amb memoria: "+f.getUltimChar());
        
        
        
      
	}

}

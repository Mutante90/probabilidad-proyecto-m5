package Buscaminas;

import java.io.IOException;
import java.util.Scanner;

/**
 * @author Justo Soria Corro
 * @since 01/03/2020
 * @version 1
 */
public class Buscaminas {

	static Scanner reader = new Scanner(System.in);

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub

		char opcio = ' ';
		boolean pipo = false;
		boolean popo = false;
		String user = " ";
		int files = 0;
		int columnes = 0;
		int mines = 0;
		char[][] tauler = new char[files][columnes];
		char[][] taulerMinat = new char[files][columnes];
		boolean ganar = false;

		String[] nombresRanking = new String[10];
		int[] puntuacionRanking = new int[10];

		do {
			opcio = presentaMenu();

			switch (opcio) {
			case '0':
				System.out.println("Adeu amics, ha!");
				break;
			case '1':
				break;
			case '2':
				System.out.println("\nJugador");
				System.out.print("Ingresa tu nombre: ");
				user = introduirNom();

				System.out.println("\nMedidas del tablero");
				System.out.print("Filas: ");
				files = introduirMida();
				System.out.print("Columnas: ");
				columnes = introduirMida();

				tauler = new char[files][columnes];
				taulerMinat = new char[files][columnes];

				System.out.println("\nDifucultad");
				System.out.print("Numero de minas (min 1 max " + ((files * columnes) - 1) + "): ");
				mines = introduirMines(files, columnes);

				pipo = true;
				popo = true;

				break;
			case '3':
				if (pipo) {
					inicialitzaTauler(tauler, files, columnes);
					minaTauler(taulerMinat, files, columnes, mines);
					mostraTauler(user, tauler, files, columnes);
					ganar = jugaJoc(tauler, taulerMinat, columnes, files, mines);
					if (!ganar) {
						actualizaRanking(nombresRanking, puntuacionRanking, user);
					}

					pipo = false;

				} else {
					System.out.println("no has editado las opciones...");
				}

				break;

			case '4':
				if (popo) {
					muestraRanking(nombresRanking, puntuacionRanking);

				} else {
					System.out.println("no has editado las opciones...");
				}
				break;
			default:
				System.out.println("no has elegido una opcion correcta...");
			}
			System.out.println("\nPulsa enter para seguir");
			System.in.read();

		} while (opcio != '0');
	}

	/**
	 * Este método es el que usamos para presentar el menú y preguntarle al usuario
	 * que opción es la que quiere.
	 * 
	 * @return opcio es la opción elegida por el usuario
	 */
	static char presentaMenu() {

		char opcion = ' ';

		System.out.println("\n\t\tBUSCAMINAS");
		System.out.println("\t\t----------");

		System.out.println("[1] Mostrar Ayuda");
		System.out.println("[2] Opciones");
		System.out.println("[3] Jugar Partida");
		System.out.println("[4] Ver Rankings");
		System.out.println("[0] Salir");

		System.out.print("Selecciona una opcion: ");
		opcion = reader.next().charAt(0);

		return opcion;
	}

	/**
	 * Este método es el que usamos para preguntarle el nombre al usuario
	 * 
	 * @return nombre es el nombre entrado por teclado
	 */
	static String introduirNom() {
		String nombre = " ";
		nombre = reader.next();

		return nombre;
	}

	/**
	 * Este método es el que usamos para preguntarle la medida del tablero al
	 * usuario (tanto fila como columnas)
	 * 
	 * @return medida es la medida entrada
	 */
	static int introduirMida() {
		int medida = 0;
		boolean pipo = false;

		while (!pipo) {
			medida = reader.nextInt();

			if (medida > 1) {
				pipo = true;
			} else {
				System.out.println("el tablero tiene que ser mas grande...");
				System.out.println("vuelve a intentarlo: ");
			}
		}
		return medida;

	}

	/**
	 * Este método es el que usamos para preguntarle al usuario las minas que quiere
	 * en el tablero (mínimo 1, máximo (filas * columnas) -1)
	 * 
	 * @param filas    son las filas especificadas por el usuario
	 * @param columnas son las columnas especificadas por el usuario
	 * @return bombas es el número de bombas entradas por el usuario
	 */
	static int introduirMines(int filas, int columnas) {
		int bombas = 0;
		boolean pipo = false;

		while (!pipo) {
			bombas = reader.nextInt();

			if (bombas > 0 && bombas < (filas * columnas)) {
				pipo = true;
			} else {
				if (bombas < 1) {
					System.out.println("no puede haber menos de 1 bomba");
				}
				if (bombas > ((filas * columnas) - 1)) {
					System.out.println("tiene que haber al menos una casilla a salvo");
				}
				System.out.println("vuelve a intentarlo");
				System.out.print("Numero de minas: ");
			}
		}

		return bombas;

	}

	/**
	 * Este método es el que usamos para crear el tablero con las medidas
	 * especificadas por el usuario de espacios en blanco
	 * 
	 * @param tablero  es el tablero con las medidas especificadas por el usuario
	 * @param filas    son las filas especificadas por el usuario
	 * @param columnas son las columnas especificadas por el usuario
	 */
	static void inicialitzaTauler(char tablero[][], int filas, int columnas) {
		for (int i = 0; i < filas; i++) {
			for (int j = 0; j < columnas; j++) {
				tablero[i][j] = ' ';
			}
		}
	}

	/**
	 * Este método es el que usamos para mostrarle el tablero al usuario
	 * 
	 * @param usuario  nombre introducido por el usuario
	 * @param tablero  es el tablero con las medidas especificadas por el usuario
	 * @param filas    son las filas especificadas por el usuario
	 * @param columnas son las columnas especificadas por el usuario
	 */
	static void mostraTauler(String usuario, char tablero[][], int filas, int columnas) {

		System.out.println("\n" + usuario);
		System.out.println();

		for (int i = 0; i < filas; i++) {
			for (int j = 0; j < columnas; j++) {
				System.out.print("[" + tablero[i][j] + "]" + " ");
			}
			System.out.println();
		}
	}

	/**
	 * Este método es el que usamos para llenar de minas el tablero de minas
	 * 
	 * @param tableroMinado es el tablero copia del introducido por el usuario que
	 *                      llenaremos de minas
	 * @param filas         son las filas especificadas por el usuario
	 * @param columnas      son las columnas especificadas por el usuario
	 * @param minas         son la cantidad de minas que ha decidido poner el
	 *                      usuario en el tablero
	 */
	static void minaTauler(char[][] tableroMinado, int filas, int columnas, int minas) {

		int filaRandom = 0;
		int columnaRandom = 0;

		for (int i = 0; i < minas; i++) {

			do {
				filaRandom = calculaFilaRandom(filas);
				columnaRandom = calculaColumnaRandom(columnas);

			} while (tableroMinado[filaRandom][columnaRandom] == '*');

			tableroMinado[filaRandom][columnaRandom] = '*';
		}
	}

	/**
	 * Este método es el que usamos para calcular un numero aleatorio entre 0 y
	 * filas length-1, este número será el que usemos para plantar las bombas en el
	 * tablero minado
	 * 
	 * @param filas son las filas especificadas por el usuario
	 * @return numero aleatorio calculado
	 */
	static int calculaFilaRandom(int filas) {

		int max = filas - 1;
		int min = 0;
		int range = max - min + 1;
		int RND = (int) (Math.random() * range) + min;

		return RND;
	}

	/**
	 * Este método es el que usamos para calcular un numero aleatorio entre 0 y
	 * columnas length-1, este número será el que usemos para plantar las bombas en
	 * el tablero minado
	 * 
	 * @param columnas son las columnas especificadas por el usuario
	 * @return numero aleatorio calculado
	 */
	static int calculaColumnaRandom(int columnas) {

		int max = columnas - 1;
		int min = 0;
		int range = max - min + 1;
		int columna = (int) (Math.random() * range) + min;

		return columna;
	}

	/**
	 * este método es el que usamos para poner en marcha todo el mecanismo de juego
	 * (darle la vuelta a la casilla elegida, ganar o perder)
	 * 
	 * @param tablero       es el tablero con las medidas especificadas por el
	 *                      usuario
	 * @param tableroMinado es el tablero copia del introducido por el usuario que
	 *                      hemos llenado de minas
	 * @param filas         son las filas especificadas por el usuario
	 * @param columnas      son las columnas especificadas por el usuario
	 * @param minas         son la cantidad de minas que ha decidido poner el
	 *                      usuario en el tablero
	 * @return boolean para saber si la partida se ha ganado o perdido
	 */
	static boolean jugaJoc(char[][] tablero, char[][] tableroMinado, int filas, int columnas, int minas) {
		int intentoFila = 0;
		int intentoColumna = 0;
		boolean pipo = false;
		int ganar = filas * columnas - minas;
		int asalvo = 0;

		while (asalvo < ganar && !pipo) {
			System.out.print("Que fila quieres: ");
			intentoFila = reader.nextInt();
			System.out.print("Que columna quieres: ");
			intentoColumna = reader.nextInt();

			if (tableroMinado[intentoFila][intentoColumna] == '*') {

				System.out.println("Explosion desmembradora!!");
				System.out.println("Orale... usted ha hacido la perdidura y la falta una pienra!!!");

				pipo = true;

				for (int i = 0; i < filas; i++) {
					for (int j = 0; j < columnas; j++) {
						System.out.print("[" + tableroMinado[i][j] + "]" + " ");
					}
					System.out.println();
				}

			} else {
				revelaRecursivo(tablero, tableroMinado, intentoFila, intentoColumna);

				asalvo++;

				for (int i = 0; i < filas; i++) {
					for (int j = 0; j < columnas; j++) {
						System.out.print("[" + tablero[i][j] + "]" + " ");
					}
					System.out.println();
				}
				if (asalvo == ganar) {
					System.out.println("Has ganado!");
				}
			}
		}
		return pipo;

	}

	/**
	 * Este método es el que usamos para mostrarle al usuario el ranking, cada
	 * victoria es un punto.
	 * 
	 * @param ranking es el vector donde guardamos los nombres de usuarios
	 * @param puntos  es el vector donde guardamos los puntos de cada usuario
	 */
	static void muestraRanking(String[] ranking, int[] puntos) {

		System.out.println("\n\tRANKING");
		System.out.println("\t-------");

		for (int i = 0; i < ranking.length; i++) {
			if (ranking[i] != null) {
				System.out.print(ranking[i]);
				System.out.println("\t\t\t" + puntos[i]);
			}
		}
	}

	/**
	 * Este método es el que usamos para actualizar el ranking según los puntos
	 * ganados
	 * 
	 * @param ranking    es el vector donde guardamos los nombres de usuarios
	 * @param puntuacion es el vector donde guardamos los puntos de cada usuario
	 * @param user       es el nombre introducido por el usuario
	 */
	private static void actualizaRanking(String[] ranking, int[] puntuacion, String user) {
		boolean semafero = false;
		int i = 0;

		while (ranking[i] != null || i > ranking.length) {
			if (ranking[i].equals(user)) {
				puntuacion[i]++;
				semafero = true;
			}
			i++;
		}
		if (i < ranking.length) {
			if (!semafero) {
				ranking[i] = user;
				puntuacion[i]++;
			}
		} else {
			System.out.println("malo malo");
		}
	}

	/**
	 * Este método es el que usamos para contar la cantidad de minas que hay
	 * alrededor de la casilla seleccionada
	 * 
	 * @param tableroMinado  es el tablero copia del introducido por el usuario que
	 *                       hemos llenado de minas
	 * @param intentoFila    es la fila seleccionada por el usuario para revelar el
	 *                       contenido
	 * @param intentoColumna es la columna seleccionada por el usuario para revelar
	 *                       el contenido
	 * @return el número de minas que ha alrededor de la casilla seleccionada
	 */
	private static int cuentaMinas(char[][] tableroMinado, int intentoFila, int intentoColumna) {

		int[] pos1 = new int[] { 1, -1, 0, 0, 1, 1, -1, -1 };
		int[] pos2 = new int[] { 0, 0, 1, -1, 1, -1, -1, 1 };
		int cont = 0;

		for (int h = 0; h < 8; h++) {
			int x = intentoColumna + pos2[h];
			int y = intentoFila + pos1[h];

			if (y < tableroMinado.length && y >= 0 && x < tableroMinado[y].length && x >= 0) {
				if (tableroMinado[y][x] == '*') {
					cont++;
				}
			}
		}
		return cont;

	}

	/**
	 * Este método es el que usamos para revelar automáticamente el contenido de las
	 * casillas que no tienen minas, esto lo hacemos usando la recursividad.
	 * 
	 * @param tablero        es el tablero con las medidas especificadas por el
	 *                       usuario
	 * @param tableroMinado  es el tablero copia del introducido por el usuario que
	 *                       hemos llenado de minas
	 * @param intentoFila    es la fila seleccionada por el usuario para revelar el
	 *                       contenido
	 * @param intentoColumna es la columna seleccionada por el usuario para revelar
	 *                       el contenido
	 */
	private static void revelaRecursivo(char[][] tablero, char[][] tableroMinado, int intentoFila, int intentoColumna) {

		int[] pos1 = new int[] { 1, -1, 0, 0, 1, 1, -1, -1 };
		int[] pos2 = new int[] { 0, 0, 1, -1, 1, -1, -1, 1 };

		int patata = cuentaMinas(tableroMinado, intentoFila, intentoColumna);

		tablero[intentoFila][intentoColumna] = Integer.toString(patata).charAt(0);

		if (patata == 0) {
			for (int h = 0; h < 8; h++) {
				int x = intentoColumna + pos2[h];
				int y = intentoFila + pos1[h];
				if (y < tableroMinado.length && y >= 0 && x < tableroMinado[y].length && x >= 0) {
					if (tablero[y][x] == ' ') {
						revelaRecursivo(tablero, tableroMinado, y, x);
					}
				}
			}
		}
	}
}
